import Vue from 'vue'

import VueRouter from 'vue-router'

import App from './components/App'

import router from './router'
import store from './store'

import './plugins'
import './bootstrap'

Vue.use(VueRouter)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
  store,
  ...App
})
