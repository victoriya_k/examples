import Vue from 'vue'

import vSelect from 'vue-select'
import toast from 'izitoast'

Vue.component('v-select', vSelect)

window.successMessage = function (message = 'Отправлено', title = 'Успех') {
  toast.success({
    title,
    message,
    position: 'bottomLeft',
    timeout: 3500
  })
}

window.errorMessage = function (message = 'Произошла какая-то ошибка', title = 'Ошибка') {
  toast.error({
    title,
    message,
    position: 'bottomLeft',
    timeout: 3500
  })
}