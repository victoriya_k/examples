export default {
  data: () => ({
    isBluetoothConnected: false,
    lastActiveHandle: null
  }),

  methods: {
    requestDevice (filter, service, characteristic) {
      this.isBluetoothConnected = true

      navigator.bluetooth.requestDevice(filter)
        .then(device => {
          return device.gatt.connect()
        })
        .then(server => {
          return server.getPrimaryService(service)
        })
        .then(service => {
          return service.getCharacteristic(characteristic)
        })
        .then(characteristic => characteristic.startNotifications())
        .then(characteristic => {
          characteristic.addEventListener('characteristicvaluechanged', this.handleNotifications)
        })
        .catch(error => {
          this.isBluetoothConnected = false
          console.log('error: ', error.message)
        })
    },
    requestDeviceWithoutNotify (filter, service, characteristic) {
      this.isBluetoothConnected = true

      navigator.bluetooth.requestDevice(filter)
        .then(device => {
          return device.gatt.connect()
        })
        .then(server => {
          return server.getPrimaryService(service)
        })
        .then(service => {
          return service.getCharacteristic(characteristic)
        })
        .then(characteristic => {
          return characteristic.readValue()
        })
        .then(characteristic => this.handleNotificationsBluetooth(characteristic))
        .catch(error => {
          this.isBluetoothConnected = false
          console.log('error: ', error.message)
        })
    },
    handleNotifications (event) {
      this.lastActiveHandle = Date.now()
      this.handleNotificationsBluetooth(event.target.value)
    },
    handleNotificationsBluetooth () {
      //
    },

    isActiveHandle () {
      return !((Date.now() - this.lastActiveHandle) > 5000)
    }
  }
}