import axios from 'axios'
import store from '../store'
import router from '../router'

axios.interceptors.request.use(request => {
  const token = store.getters['auth/token']

  if (token) {
    request.headers.common['Authorization'] = `Bearer ${token}`
  }

  return request
})

axios.interceptors.response.use(
  response => {
    return response
  },

  error => {
    if (error.response.status === 401 && store.getters['auth/token']) {
      localStorage.removeItem('token')
      router.push('/login')
    }

    return Promise.reject(error)
  }
)