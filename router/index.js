import VueRouter from 'vue-router'
import routes from './routes'
import store from '../store'

const router = createRouter()

export default router

function createRouter () {
  const router = new VueRouter({
    scrollBehavior,
    mode: 'history',
    routes
  })

  router.afterEach(afterEach)
  router.beforeEach(beforeEach)

  return router
}

function beforeEach (to, from, next) {
  let loggedIn = store.getters['auth/token']

  window.prevRoute = from

  let meta = to.meta

  if (meta.guestRoute !== true && !loggedIn) {
    return next('/login')
  } else {
    if (to.path === '/') {
      return next('/dashboard')
    } else if (loggedIn && to.path === '/login') {
      return next('/dashboard')
    }
  }

  let components = router
    .getMatchedComponents({ ...to })
    .map(component => {
      return typeof component === 'function' ? component() : component
    })

  router.app.$nextTick(() => {
    console.log(components)

    router.app.$loading.start()
    router.app.setLayout(components[0].layout || '')
  })

  next()
}

function afterEach (to, from, next) {
  router.app.$nextTick(() => router.app.$loading.finish())
}

function scrollBehavior (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  }

  if (to.hash) {
    return { selector: to.hash }
  }

  const [component] = router.getMatchedComponents({ ...to }).slice(-1)

  if (component && component.scrollToTop === false) {
    return {}
  }

  return { x: 0, y: 0 }
}