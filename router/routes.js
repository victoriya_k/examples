import Registration from '../pages/auth/registration'
import Login from '../pages/auth/login'

import Dashboard from '../pages/dashboard'
import Weights from '../pages/weights'
import Settings from '../pages/settings'
import Clients from '../pages/clients'
import clientsView from '../pages/clientsView'

import PageNotFound from '../pages/pagenotfound'

export default [
  { path: '/dashboard', name: 'dashboard', component: Dashboard },
  { path: '/history-weights', name: 'weights', component: Weights },
  { path: '/settings', name: 'settings', component: Settings },
  { path: '/clients', name: 'clients', component: Clients },
  { path: '/clients/3', name: 'clientsView', component: clientsView },

  { path: '/login', name: 'login', component: Login, meta: { guestRoute: true } },
  { path: '/registration', name: 'registration', component: Registration, meta: { guestRoute: true } },

  { path: '*', component: PageNotFound }
]
