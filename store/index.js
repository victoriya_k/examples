import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import products from './modules/products'
import historyWeights from './modules/historyWeights'
import historyEnergy from './modules/historyEnergy'
import historyWaters from './modules/historyWaters'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    products,
    historyWeights,
    historyEnergy,
    historyWaters
  }
})