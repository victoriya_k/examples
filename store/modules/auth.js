import axios from 'axios'
import * as types from '../mutation-types'

export default {
  namespaced: true,

  state: {
    user: null,
    token: localStorage.getItem('token')
  },

  getters: {
    user: state => state.user,
    token: state => state.token,
    check: state => state.user !== null
  },

  mutations: {
    [types.SAVE_TOKEN] (state, token) {
      state.token = token
      localStorage.setItem('token', token)
    },

    [types.LOGOUT] (state) {
      state.user = null
      state.token = null

      localStorage.removeItem('token')
    },

    [types.FETCH_USER] (state, { user }) {
      state.user = user
    }
  },

  actions: {
    saveToken ({ commit }, token) {
      commit(types.SAVE_TOKEN, token)
    },

    fetchUser ({ commit }) {
      axios.get('/api/user-info')
        .then(res => commit(types.FETCH_USER, { user: res.data }))
    },

    updateUser ({ commit, state }, payload) {
      return axios.put('/api/user-info/' + state.user.id, payload)
        .then(res => commit(types.FETCH_USER, { user: res.data }))
    },

    async logout ({ commit }) {
      try {
        await axios.post('/api/logout')
      } catch (e) {
      }

      commit(types.LOGOUT)
    }
  }
}
