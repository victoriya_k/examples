import axios from 'axios'
import moment from 'moment'
import * as types from '../mutation-types'

export default {
  namespaced: true,

  state: {
    items: []
  },

  getters: {
    items: state => state.items,
    todayItems: state => {
      const today = moment()

      return state
        .items
        .filter(item => {
          return moment(item.created_at).isSame(today, 'day')
        })
    },
    sumToday: state => {
      const today = moment()

      return state
        .items
        .filter(item => {
          return moment(item.created_at).isSame(today, 'day')
        })
        .reduce((acc, item) => {
          acc.energy += item.energy
          acc.protein += item.protein
          acc.carbohydrate += item.carbohydrate
          acc.fat += item.fat

          return acc
        }, { energy: 0, protein: 0, carbohydrate: 0, fat: 0 })
    }
  },

  mutations: {
    [types.STORE_HISTORY_ENERGY] (state, newItem) {
      state.items.push(newItem)
    },
    [types.FETCH_HISTORY_ENERGIES] (state, energies) {
      state.items = energies
    }
  },

  actions: {
    save ({ commit }, payload) {
      axios
        .post('/api/history-energies', payload)
        .then(res => commit(types.STORE_HISTORY_ENERGY, res.data))
    },
    fetchHistory ({ commit }) {
      axios
        .get('/api/history-energies')
        .then(res => commit(types.FETCH_HISTORY_ENERGIES, res.data))
    }
  }
}