import axios from 'axios'
import moment from 'moment'
import * as types from '../mutation-types'

export default {
  namespaced: true,

  state: {
    items: []
  },

  getters: {
    items: state => state.items,
    sumToday: state => {
      const today = moment()

      return state
        .items
        .filter(item => {
          return moment(item.created_at).diff(today, 'days') === 0
        })
        .reduce((acc, item) => (acc + item.quantity), 0)
    }

  },

  mutations: {
    [types.STORE_HISTORY_WATER] (state, newItem) {
      state.items.push(newItem)
    },
    [types.FETCH_HISTORY_WATERS] (state, weights) {
      state.items = weights
    }
  },

  actions: {
    save ({ commit }, payload) {
      axios
        .post('/api/history-waters', payload)
        .then(res => commit(types.STORE_HISTORY_WATER, res.data))
    },
    fetchHistory ({ commit }) {
      axios
        .get('/api/history-waters')
        .then(res => commit(types.FETCH_HISTORY_WATERS, res.data))
    }
  }
}