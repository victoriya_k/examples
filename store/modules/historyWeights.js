import axios from 'axios'
import moment from 'moment'
import uniqBy from 'lodash/uniqBy'
import * as types from '../mutation-types'

export default {
  namespaced: true,

  state: {
    items: []
  },

  getters: {
    items: state => state.items,
    lastWeekItems: state =>
      uniqBy(
        state
          .items
          .map(item => ({
            label: moment(item.created_at).format('DD/MM'),
            value: item.weight
          })),
        'label'
      )
        .slice(0, 7),
    graphicItems: state =>
      uniqBy(
        state
          .items
          .map(item => ({
            label: moment(item.created_at).format('DD/MM'),
            value: item.weight
          })),
        'label'
      )
  },

  mutations: {
    [types.STORE_HISTORY_WEIGHT] (state, newItem) {
      state.items.push(newItem)
    },
    [types.FETCH_HISTORY_WEIGHTS] (state, weights) {
      state.items = weights
    }
  },

  actions: {
    save ({ commit }, payload) {
      return axios
        .post('/api/history-weights', payload)
        .then(res => commit(types.STORE_HISTORY_WEIGHT, res.data))
    },
    fetchHistory ({ commit }) {
      axios
        .get('/api/history-weights')
        .then(res => commit(types.FETCH_HISTORY_WEIGHTS, res.data))
    }
  }
}
