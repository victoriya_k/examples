import axios from 'axios'
import * as types from '../mutation-types'

export default {
  namespaced: true,

  state: {
    items: []
  },

  getters: {
    items: state => state.items
  },

  mutations: {
    [types.FETCH_PRODUCTS] (state, products) {
      state.items = products
    }
  },

  actions: {
    fetchProducts ({ commit }, payload) {
      axios.get('/api/products?search=' + payload.search)
        .then(res => commit(types.FETCH_PRODUCTS, res.data))
    }
  }
}